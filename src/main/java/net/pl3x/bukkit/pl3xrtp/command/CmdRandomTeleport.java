package net.pl3x.bukkit.pl3xrtp.command;

import net.pl3x.bukkit.pl3xrtp.Pl3xRTP;
import net.pl3x.bukkit.pl3xrtp.configuration.Config;
import net.pl3x.bukkit.pl3xrtp.configuration.Lang;
import net.pl3x.bukkit.pl3xrtp.task.RTPCooldown;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CmdRandomTeleport implements TabExecutor {
    private final Pl3xRTP plugin;
    private final ThreadLocalRandom random;

    public CmdRandomTeleport(Pl3xRTP plugin) {
        this.plugin = plugin;
        this.random = ThreadLocalRandom.current();
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 1) {
            return Stream.of("reload", "short", "medium", "far", "veryfar")
                    .filter(name -> name.startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length == 0) {
            Lang.send(sender, Lang.VERSION
                    .replace("{plugin}", plugin.getName())
                    .replace("{version}", plugin.getDescription().getVersion()));
            return false; // send usage
        }

        if (args[0].equalsIgnoreCase("reload")) {
            if (!sender.hasPermission("command.rtp.reload")) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }

            Config.reload(plugin);
            Lang.reload(plugin);

            Lang.send(sender, Lang.RELOAD
                    .replace("{plugin}", plugin.getName())
                    .replace("{version}", plugin.getDescription().getVersion()));
            return true;
        }

        if (!(sender instanceof Player)) {
            Lang.send(sender, Lang.PLAYER_COMMAND);
            return true;
        }

        Player player = (Player) sender;
        final UUID uuid = player.getUniqueId();

        if (RTPCooldown.cooldown.containsKey(uuid)) {
            Lang.send(sender, Lang.COOLDOWN
                    .replace("{remaining}", secondsToReadable(RTPCooldown.cooldown.get(uuid))));
            return true;
        }

        int min, max;
        if (args[0].equalsIgnoreCase("short")) {
            if (!sender.hasPermission("command.rtp.short")) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }
            min = Config.SHORT_MIN;
            max = Config.SHORT_MAX;
        } else if (args[0].equalsIgnoreCase("medium")) {
            if (!sender.hasPermission("command.rtp.medium")) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }
            min = Config.MEDIUM_MIN;
            max = Config.MEDIUM_MAX;
        } else if (args[0].equalsIgnoreCase("far")) {
            if (!sender.hasPermission("command.rtp.far")) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }
            min = Config.FAR_MIN;
            max = Config.FAR_MAX;
        } else if (args[0].equalsIgnoreCase("veryfar")) {
            if (!sender.hasPermission("command.rtp.veryfar")) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }
            min = Config.VERYFAR_MIN;
            max = Config.VERYFAR_MAX;
        } else {
            Lang.send(sender, Lang.UNKNOWN_COMMAND);
            return false; // show usage
        }

        int counter = 0;
        Location origin = Config.CENTER_ON_PLAYER ? player.getLocation() : player.getWorld().getSpawnLocation();
        Location destination = null;

        while (destination == null) {
            if (counter > Config.MAX_TRIES_LIMIT) {
                Lang.send(sender, Lang.MAX_TRIES_LIMIT);
                return true;
            }
            counter++;

            double angle = random.nextDouble() * 2 * Math.PI;
            double offsetX = Math.cos(angle) * random.nextInt(min, max + 1);
            double offsetZ = Math.sin(angle) * random.nextInt(min, max + 1);

            destination = plugin.getValidLocation(player, player.getWorld(),
                    (int) (origin.getBlockX() + offsetX),
                    (int) (origin.getBlockZ() + offsetZ));
        }

        if (Config.TELEPORT_SOUND != null) {
            origin.getWorld().playSound(origin, Config.TELEPORT_SOUND, 1F, 1F);
            destination.getWorld().playSound(destination, Config.TELEPORT_SOUND, 1F, 1F);
        }

        player.teleport(destination);
        Lang.send(sender, Lang.TELEPORTED);

        RTPCooldown.cooldown.put(uuid, Config.COOLDOWN);

        RTPCooldown rtpCooldownTask = new RTPCooldown(uuid);
        rtpCooldownTask.runTaskTimer(plugin, 20, 20);
        return true;
    }

    private String secondsToReadable(int seconds) {
        return String.format("%d:%02d", seconds / 60, seconds % 60);
    }
}

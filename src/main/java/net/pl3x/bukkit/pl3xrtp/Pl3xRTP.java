package net.pl3x.bukkit.pl3xrtp;

import net.pl3x.bukkit.pl3xrtp.command.CmdRandomTeleport;
import net.pl3x.bukkit.pl3xrtp.configuration.Config;
import net.pl3x.bukkit.pl3xrtp.configuration.Lang;
import net.pl3x.bukkit.pl3xrtp.hook.Pl3xClaimsHook;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xRTP extends JavaPlugin {
    private Pl3xClaimsHook pl3xClaimsHook;

    @Override
    public void onEnable() {
        Config.reload(this);
        Lang.reload(this);

        if (getServer().getPluginManager().isPluginEnabled("Pl3xClaims")) {
            pl3xClaimsHook = new Pl3xClaimsHook(getServer().getPluginManager().getPlugin("Pl3xClaims"));
        }

        getCommand("rtp").setExecutor(new CmdRandomTeleport(this));

        getLogger().info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        pl3xClaimsHook = null;

        getLogger().info(getName() + " disabled.");
    }

    public Location getValidLocation(Player player, World world, int x, int z) {
        Block block = world.getHighestBlockAt(x, z);
        if (block.getRelative(BlockFace.DOWN).isLiquid()) {
            return null; // do not teleport to liquids
        }

        Location location = block.getLocation().add(0.5, 0, 0.5); // center the location so we dont end up in a wall

        if (pl3xClaimsHook != null && !pl3xClaimsHook.isAllowedToBuild(location, player)) {
            return null; // cannot build in this claim
        }

        if (!location.getWorld().getWorldBorder().isInside(location)) {
            return null; // outside of world border
        }

        return location;
    }
}

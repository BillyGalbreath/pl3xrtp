package net.pl3x.bukkit.pl3xrtp.task;

import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class RTPCooldown extends BukkitRunnable {
    public static final Map<UUID, Integer> cooldown = new HashMap<>();

    private final UUID uuid;

    public RTPCooldown(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public void run() {
        if (!cooldown.containsKey(uuid)) {
            cancel();
            return;
        }

        int current = cooldown.get(uuid) - 1;

        if (current <= 0) {
            cooldown.remove(uuid);
            cancel();
            return;
        }

        cooldown.put(uuid, current);
    }
}

package net.pl3x.bukkit.pl3xrtp.configuration;

import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Config {
    public static String LANGUAGE_FILE = "lang-en.yml";

    public static Sound TELEPORT_SOUND = Sound.ENTITY_ENDERMEN_TELEPORT;

    public static int MAX_TRIES_LIMIT = 100;
    public static int COOLDOWN = 3600;

    public static boolean CENTER_ON_PLAYER = true;

    public static int SHORT_MIN = 100;
    public static int SHORT_MAX = 500;
    public static int MEDIUM_MIN = 500;
    public static int MEDIUM_MAX = 1000;
    public static int FAR_MIN = 1000;
    public static int FAR_MAX = 2000;
    public static int VERYFAR_MIN = 2000;
    public static int VERYFAR_MAX = 5000;

    public static void reload(JavaPlugin plugin) {
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");

        String soundName = config.getString("teleport-sound", "ENTITY_ENDERMEN_TELEPORT").toUpperCase();
        try {
            TELEPORT_SOUND = Sound.valueOf(soundName);
        } catch (IllegalArgumentException e) {
            TELEPORT_SOUND = Sound.ENTITY_ENDERMEN_TELEPORT;
        }

        MAX_TRIES_LIMIT = config.getInt("max-tries-limit", 100);
        COOLDOWN = config.getInt("cooldown", 3600);
        CENTER_ON_PLAYER = config.getBoolean("center-on-player", true);

        SHORT_MIN = config.getInt("distance.short.min", 100);
        SHORT_MAX = config.getInt("distance.short.max", 500);
        MEDIUM_MIN = config.getInt("distance.medium.min", 500);
        MEDIUM_MAX = config.getInt("distance.medium.max", 1000);
        FAR_MIN = config.getInt("distance.far.min", 1000);
        FAR_MAX = config.getInt("distance.far.max", 2000);
        VERYFAR_MIN = config.getInt("distance.veryfar.min", 2000);
        VERYFAR_MAX = config.getInt("distance.veryfar.max", 5000);
    }
}

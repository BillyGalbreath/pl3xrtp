package net.pl3x.bukkit.pl3xrtp.configuration;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Lang {
    public static String COMMAND_NO_PERMISSION = "&4You do not have permission for this command!";
    public static String PLAYER_COMMAND = "&4Player only command!";
    public static String UNKNOWN_COMMAND = "&4Unknown command!";
    public static String MAX_TRIES_LIMIT = "&4Could not find a safe spot to teleport to! Try again in a few moments.";
    public static String TELEPORTED = "&dTeleported to random location.";
    public static String COOLDOWN = "&4You cannot use this command for another &7{remaining}";
    public static String VERSION = "&d{plugin} v{version}.";
    public static String RELOAD = "&d{plugin} v{version} reloaded.";

    public static void reload(JavaPlugin plugin) {
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        plugin.saveResource(Config.LANGUAGE_FILE, false);
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        COMMAND_NO_PERMISSION = config.getString("command-no-permission", "&4You do not have permission for this command!");
        PLAYER_COMMAND = config.getString("player-command", "&4Player only command!");
        UNKNOWN_COMMAND = config.getString("unknown-command", "&4Unknown command!");
        MAX_TRIES_LIMIT = config.getString("max-tries-limit", "&4Could not find a safe spot to teleport to! Try again in a few moments.");
        TELEPORTED = config.getString("teleported", "&dTeleported to random location.");
        COOLDOWN = config.getString("cooldown", "&4You cannot use this command for another &7{remaining}");
        VERSION = config.getString("version", "&d{plugin} v{version}.");
        RELOAD = config.getString("reload", "&d{plugin} v{version} reloaded.");
    }

    public static void send(CommandSender recipient, String message) {
        if (message == null) {
            return; // do not send blank messages
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        if (ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }
}

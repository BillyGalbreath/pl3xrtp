package net.pl3x.bukkit.pl3xrtp.hook;

import net.pl3x.bukkit.claims.Pl3xClaims;
import net.pl3x.bukkit.claims.claim.Claim;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class Pl3xClaimsHook {
    private final Pl3xClaims pl3xClaims;

    public Pl3xClaimsHook(Plugin pl3xClaims) {
        this.pl3xClaims = (Pl3xClaims) pl3xClaims;
    }

    public boolean isAllowedToBuild(Location location, Player player) {
        Claim claim = pl3xClaims.getClaimManager().getClaim(location);
        return claim == null || claim.allowBuild(player);
    }
}
